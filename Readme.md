# Command

## Docker

docker build -t myorg/myapp .
docker run -d -p 8080:8080 myorg/myapp

## OpenShift

oc new-project test // create a project called test
oc project test // switch to the project test

oc new-app dockerhub-address:tagname //create the docker with image

minishift oc-env
